/**
 * @description  This is independent custom module to be used for reading the QR Code
 * @author Dipesh KC 
 * @date 9/26/2014
 */

angular.module('QRUtility', [])
/**
 * QRUtility factory
 * is of asynchronous nature and returns  promise
 *
 * Available methods:
 * 1. scan
 * This is asynchronous function, return promise
 *   
 */
.factory('QRUtility', ['$q',function($q) {

	var _private={},
		_public={}
	;
	_public.scan = function(options){
	    var q = $q.defer();
	    // requires plugin :     cordova plugin add com.google.cordova.admob
		// link     :     https://github.com/floatinghotpot/cordova-admob-pro
		try{

	        cordova.plugins.barcodeScanner.scan(function (result) {
	          q.resolve(result);
	        }, function (err) {
	          q.reject(err);
	        });

		}catch(e) {
			q.reject(e);
		}


        return q.promise;
    
	};

	return _public;
}]);
