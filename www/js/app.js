var _APP_ = 'QRCodeReader';
angular.module(_APP_, [
  'ionic',
  'QRUtility'
])

.constant('APP_NAME', 'QRCodeReader')
.constant('APP_VERSION', '1.0')

.controller('AppCtrl', ['$scope','QRUtility',
  function ($scope,QRUtility) {
    $scope.result = null;
    $scope.scan = function(){
      QRUtility.scan()
      .then(function(result){
        console.log(result);
        try {
          var response = result;
          
          if( response.cancelled ) {

            $scope.result = 'Cancelled by user!';  

          }else if(response.text){
            
            delete response.cancelled;
            delete response.format;
            
            try{
              
              var jsonResult = JSON.parse(response.text);
              $scope.result = "";
              angular.forEach(jsonResult,function(value,index) {
                $scope.result += index + " = " + value;
              });

              //$scope.result = jsonResult; 
            
            }catch(e) {
              console.error(e);
              $scope.result = response.text;    
            
            }
            
          }else{

            $scope.result = "No text found!";  

          }

        }catch(e) {

          $scope.result = "Invalid data \n";
          $scope.result +=e;

        }

      },function(err){
        console.error(err);
        $scope.result = "QRCode cannot be accessed in this device!";
      })
      

    };


  }
]);