#Barcode Scanner Project


##Dev Requirements

1. Install nodejs npm package manager
2. Install following packages globally
	cordova,bower,ionic

###Setup

Use command ```npm install``` to install the dependencies automaticlly
User command ```bower install```

###Run

To add platform
	```cordova platform add android```
	```cordova platform add ios```

To run in your device
	```cordova platform run ios```
	```cordova platform run android```